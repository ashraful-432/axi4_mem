`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 06/30/2019 12:34:43 PM
// Design Name:
// Module Name: test
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
//
`include "AXI4_config.h"

interface axi_intf
#(parameter _ID_WIDTH    = 2,
parameter _AWIDTH      = 32,
parameter _STRB_SIZE  = 4,
parameter _DBUS_WIDTH = 32)
(input axi_clk, input axi_rst_n);

//logic                   axi_awlock;
//logic                   axi_awcache;
//logic                   axi_awprot;
logic [_ID_WIDTH-1:0]   axi_awid;
logic [1:0]             axi_awburst;
logic [7:0]             axi_awlen;
logic [2:0]             axi_awsize;
logic [_AWIDTH - 1: 0]  axi_awaddr;
logic                   axi_awvalid;
logic                   axi_awready;

//logic                   axi_wid;
logic                   axi_wlast;
logic [_STRB_SIZE-1: 0] axi_wstrb;
logic [_DBUS_WIDTH-1:0] axi_wdata;
logic                   axi_wvalid;
logic                   axi_wready;

logic [_ID_WIDTH-1:0]   axi_bid;
logic [1 : 0]           axi_bresp;
logic                   axi_bvalid;
logic                   axi_bready;

//logic                   axi_arlock;
//logic                   axi_arcache;
//logic                   axi_arprot;
logic [_ID_WIDTH-1:0]   axi_arid;
logic [1:0]             axi_arburst;
logic [7:0]             axi_arlen;
logic [2:0]             axi_arsize;
logic [_AWIDTH - 1 : 0] axi_araddr;
logic                   axi_arvalid;
logic                   axi_arready;

logic [_ID_WIDTH-1:0]   axi_rid;
logic [1 : 0]           axi_rresp;
logic                   axi_rlast;
logic [_DBUS_WIDTH-1:0] axi_rdata;
logic                   axi_rvalid;
logic                   axi_rready;

modport axi_master(
	input axi_clk,
	input axi_rst_n,
  output axi_awid,
  output axi_awburst,
  output axi_awlen,
  output axi_awsize,
  output axi_awaddr,
  output axi_awvalid,
  input  axi_awready,

  output axi_wlast,
  output axi_wstrb,
  output axi_wdata,
  output axi_wvalid,
  input  axi_wready,

  input  axi_bid,
  input  axi_bresp,
  input  axi_bvalid,
  output axi_bready,

  output axi_arid,
  output axi_arburst,
  output axi_arlen,
  output axi_arsize,
  output axi_araddr,
  output axi_arvalid,
  input  axi_arready,

  input  axi_rid,
  input  axi_rresp,
  input  axi_rlast,
  input  axi_rdata,
  input  axi_rvalid,
  output axi_rready
);

modport axi_slave(
	input axi_clk,
	input axi_rst_n,
  input  axi_awid,
  input  axi_awburst,
  input  axi_awlen,
  input  axi_awsize,
  input  axi_awaddr,
  input  axi_awvalid,
  output axi_awready,

  input  axi_wlast,
  input  axi_wstrb,
  input  axi_wdata,
  input  axi_wvalid,
  output axi_wready,

  output axi_bid,
  output axi_bresp,
  output axi_bvalid,
  input  axi_bready,

  input  axi_arid,
  input  axi_arburst,
  input  axi_arlen,
  input  axi_arsize,
  input  axi_araddr,
  input  axi_arvalid,
  output axi_arready,

  output axi_rid,
  output axi_rresp,
  output axi_rlast,
  output axi_rdata,
  output axi_rvalid,
  input  axi_rready
);

   task automatic init_axi_master;
   begin
      axi_awid    = 0;
      axi_awburst = 0;
      axi_awlen   = 0;
      axi_awsize  = 0;
      axi_awaddr  = 0;
      axi_awvalid = 0;

      axi_wlast   = 0;
      axi_wstrb   = 0;
      axi_wdata   = 0;
      axi_wvalid  = 0;

      axi_bready  = 0;

      axi_arid    = 0;
      axi_arburst = 0;
      axi_arlen   = 0;
      axi_arsize  = 0;
      axi_araddr  = 0;
      axi_arvalid = 0;

      axi_rready  = 0;
   end
   endtask

   task automatic init_axi_slave;
   begin
      axi_awready = 0;

      axi_wready  = 0;

      axi_bid     = 0;
      axi_bresp   = 0;
      axi_bvalid  = 0;

      axi_arready = 0;

      axi_rid     = 0;
      axi_rresp   = 0;
      axi_rlast   = 0;
      axi_rdata   = 0;
      axi_rvalid  = 0;
   end
   endtask

   clocking master_clocking @ (posedge axi_clk);
    	default input #1step output negedge;
    	output axi_awid;
    	output axi_awburst;
    	output axi_awlen;
    	output axi_awsize;
    	output axi_awaddr;
    	output axi_awvalid;
    	input  axi_awready;

    	output axi_wlast;
    	output axi_wstrb;
    	output axi_wdata;
    	output axi_wvalid;
    	input  axi_wready;

    	input  axi_bid;
    	input  axi_bresp;
    	input  axi_bvalid;
    	output axi_bready;

    	output axi_arid;
    	output axi_arburst;
    	output axi_arlen;
    	output axi_arsize;
    	output axi_araddr;
    	output axi_arvalid;
    	input  axi_arready;

    	input  axi_rid;
    	input  axi_rresp;
    	input  axi_rlast;
    	input  axi_rdata;
    	input  axi_rvalid;
    	output axi_rready;
   endclocking

   clocking slave_clocking @ (posedge axi_clk);
      default input #1step output negedge;
      input  axi_awid;
      input  axi_awburst;
      input  axi_awlen;
      input  axi_awsize;
      input  axi_awaddr;
      input  axi_awvalid;
      output axi_awready;

      input  axi_wlast;
      input  axi_wstrb;
      input  axi_wdata;
      input  axi_wvalid;
      output axi_wready;

      output axi_bid;
      output axi_bresp;
      output axi_bvalid;
      input  axi_bready;

      input  axi_arid;
      input  axi_arburst;
      input  axi_arlen;
      input  axi_arsize;
      input  axi_araddr;
      input  axi_arvalid;
      output axi_arready;

      output axi_rid;
      output axi_rresp;
      output axi_rlast;
      output axi_rdata;
      output axi_rvalid;
      input  axi_rready;
   endclocking

endinterface

typedef struct {
        int AWIDTH;
	int DWIDTH;
	int RAM_DEPTH;
} _defs_dpi;


typedef struct {
        bit [1:0] burst_type;
	bit [7:0] burst_length;
}axi_config;

//program axi_master_model(axi_intf.axi_master m_axi); //Modelsim PE do not
//support Program block
module axi_master_model(axi_intf m_axi);

	import "DPI-C" context task c_axi_write(input _defs_dpi defs, input int mode, input int addr, input int tst_data, input int Burst_Length);
   //import "DPI-C" context task c_axi_read (input int addr, output int tst_data, input int n_transaction);
   	import "DPI-C" context task c_axi_read (input _defs_dpi defs, input int mode, input int addr, input int expected_data, input int Burst_Length, output int fail);
   	// Write task
   	export "DPI-C" task wait_negedge;
   	export "DPI-C" task wait_posedge;
   	export "DPI-C" task assert_aw_channel;
   	export "DPI-C" task wait_for_awready;
   	export "DPI-C" task deassert_aw_channel;
   	export "DPI-C" task assert_wr_channel;
   	export "DPI-C" task wait_for_wready;
   	export "DPI-C" task deassert_wr_channel;
   	export "DPI-C" task assert_bready;
   	export "DPI-C" task wait_for_bvalid;
   	export "DPI-C" task deassert_bready;
   	// Read task
   	export "DPI-C" task assert_ar_channel;
   	export "DPI-C" task wait_for_arready;
   	export "DPI-C" task deassert_ar_channel;
   	export "DPI-C" task assert_rready;
   	export "DPI-C" task cap_rd_data;
   	export "DPI-C" task check_rlast;
   	export "DPI-C" task deassert_rready;

	class axi_master;
		//virtual interface axi_intf.axi_master m_axi_;
		//virtual interface axi_intf m_axi_;
   		logic [`AWIDTH - 1:0]  addr;
   		logic [DBUS_WIDTH-1:0] data;
   		axi_config mst_cfg;
   		_defs_dpi deflist;

   		//function new(virtual interface axi_intf mst_axi);
		function new();
   		begin
			//this.m_axi_ = mst_axi;
      			deflist.AWIDTH = `AWIDTH;
      			deflist.DWIDTH = `DWIDTH;
      			deflist.RAM_DEPTH = `RAM_DEPTH;
			//m_axi_.init_axi_master();
   		end
   		endfunction
    
   		task test;
   		output int tst_fail;
		//virtual interface axi_intf.axi_master m_axi_;
   		input int tst_addr;
   		input int tst_data; 
  		begin
      			addr = tst_addr;
      			data = tst_data;
      			addr = { addr[`AWIDTH-1:`DWIDTH], {`DWIDTH{1'b0}} };
      			@(negedge m_axi.axi_clk);
      			c_axi_write(deflist, mst_cfg.burst_type, addr, data, mst_cfg.burst_length);
      			#25;
      			@(negedge m_axi.axi_clk);
      			c_axi_read(deflist, mst_cfg.burst_type, tst_addr, tst_data, mst_cfg.burst_length, tst_fail); 
      			if(tst_fail>0)
      			begin
         			$display("Error: Mismatch in AXI4 read");
      			end
   		end
   		endtask  
   	endclass : axi_master

   	task wait_negedge;
   	begin
    		@(negedge m_axi.axi_clk);
   	end
   	endtask
   
   	task wait_posedge;
   	begin
    		@(posedge m_axi.axi_clk);
   	end
   	endtask
   
   	task assert_aw_channel;
   	input int addr;
   	input int awid;//logic [ID_WIDTH-1:0] awid;
   	input int awlen;//logic [7:0] awlen;
   	input int awsize;//logic [2:0] awsize;
   	input int awburst;//logic [2:0] awburst;
   	begin
    		$display("TASK: assert_aw_channel:: Addr = 0x%4x Awid = 0x%4x Awlen = 0x%x Awsize = 0x%x Awburst = 0x%x Time = %d\n",
                  addr, awid, awlen, awsize, awburst, $time);
    		//@(negedge m_axi.axi_clk);
    		m_axi.axi_awaddr  = addr;
    		m_axi.axi_awid    = awid;         
    		m_axi.axi_awvalid = 1;
    		m_axi.axi_awlen   = awlen;
    		m_axi.axi_awsize  = awsize;
    		m_axi.axi_awburst = awburst;
   	end
   	endtask  
   
   	task wait_for_awready;
   	automatic int time_out = 0;
   	begin
   	do
    	begin
        	@(posedge m_axi.axi_clk);
        	time_out = time_out+1;
        	if(time_out>64)
        	begin
           		$display("TASK: wait_for_awready:: Timeout without asserting slaves AXI AWREADY\n");
           		$stop;
        	end
    	end while(m_axi.axi_awready != 1);
    	time_out = 0;
   	end
   	endtask
   
   	task deassert_aw_channel;
   	begin
    		@(negedge m_axi.axi_clk);
    		m_axi.axi_awaddr  = 'bX;
    		m_axi.axi_awid    = 'bX;         
    		m_axi.axi_awvalid = 0;
    		m_axi.axi_awlen   = 'bX;
    		m_axi.axi_awsize  = 'bX;
    		m_axi.axi_awburst = 'bX;         
   	end
   	endtask
   
   	task assert_wr_channel;
   	input int data;//[DBUS_WIDTH-1:0] data;
   	input int strobe;
   	input logic wlast;
   	begin
    		m_axi.axi_wvalid = 1;         
    		m_axi.axi_wdata = data;
    		m_axi.axi_wstrb = strobe;
    		m_axi.axi_wlast = wlast;
   	end
   	endtask
   
   	task wait_for_wready;
   	automatic int time_out=0;
   	begin
    		do
    		begin
        		time_out = time_out+1;
        		if(time_out>64)
        		begin
            			$display("TASK: wait_for_wready:: Timeout without asserting slaves AXI WREADY\n");
            			$stop;
        		end
        		@(posedge m_axi.axi_clk);
    		end while(m_axi.axi_wready != 1);
    		time_out = 0;
   	end
   	endtask
   
   	task deassert_wr_channel;
   	begin
    		@(negedge m_axi.axi_clk);
    		m_axi.axi_wvalid = 0;         
    		m_axi.axi_wdata = 'bX;
    		m_axi.axi_wstrb = 'bX;
    		m_axi.axi_wlast = 0;  
   	end
   	endtask
   
   	task assert_bready;
   	begin
    		//@(negedge m_axi.axi_clk);
    		m_axi.axi_bready <=1;
   	end
   	endtask
   
   	task wait_for_bvalid;
   	automatic int time_out;
   	begin
    		do
    		begin
        		@(posedge m_axi.axi_clk);
        		time_out = time_out+1;
        		if(time_out>64)
        		begin
            			$display("TASK: wait_for_bvalid:: Timeout without asserting slaves AXI BVALID\n");
            			$stop;
        		end
    		end while(m_axi.axi_bvalid != 1);
   	end
   	endtask
   
   	task deassert_bready;
   	begin
    		@(negedge m_axi.axi_clk);
    		m_axi.axi_bready <=0;
   	end
   	endtask

   	task assert_ar_channel;
   	input int address;
   	input int arid;//logic [ID_WIDTH-1:0] arid;
   	input int arlen;//logic [7:0] arlen;
   	input int arsize;//logic [2:0] arsize;
   	input int arburst;//logic [2:0] arburst;
   	begin
    		m_axi.axi_araddr  = address;
    		m_axi.axi_arid    = arid;
    		m_axi.axi_arlen   = arlen;
    		m_axi.axi_arsize  = arsize;
    		m_axi.axi_arburst = arburst;
    		m_axi.axi_arvalid = 1;
   	end
   	endtask

   	task wait_for_arready;
   	automatic int time_out = 0;
   	begin
      		do
      		begin
         		time_out = time_out+1;
         		if(time_out>64)
         		begin
           			$display("TASK: single_read:: Timeout without asserting slaves AXI ARREADY\n");
           			$stop;
         		end
         		@(posedge m_axi.axi_clk);         
      		end while(m_axi.axi_arready != 1);
      		time_out = 0;    
   	end
   	endtask

   	task deassert_ar_channel;
   	begin
    		@(negedge m_axi.axi_clk);
    		m_axi.axi_araddr  = 'bX;
    		m_axi.axi_arvalid = 0;
   	end
   	endtask

   	task assert_rready;
   	begin
     		m_axi.axi_rready = 1;
   	end
   	endtask

   	task cap_rd_data;
   	output int data;
   	automatic int time_out = 0;
   	begin
      		do
      		begin
         		time_out = time_out+1;
         		if(time_out>64)
         		begin
           			$display("TASK: single_read:: Timeout without asserting slaves AXI RVALID\n");
           			$stop;
         		end
         		@(posedge m_axi.axi_clk);         
      		end while(m_axi.axi_rvalid != 1);
      		if(m_axi.axi_rresp != 2'b00) 
      		begin
	    		$display("Error: Read response return non zero values (%x) ", m_axi.axi_rresp);
            		$stop;
      		end
      		data = m_axi.axi_rdata;
   	end
   	endtask

   	task check_rlast;
   	begin
      		if(m_axi.axi_rlast != 1)
      		begin
         		$display("TASK: single_read:: Eroor not asserted slaves AXI RLAST \n");
         		$stop;
      		end
   	end
   	endtask

   	task deassert_rready;
   	begin
     		@(negedge m_axi.axi_clk);
     		m_axi.axi_rready = 1'b0;
   	end
   	endtask	
	
	axi_master mst;
	int fail;
	int tst_addr;
	int tst_data;

	initial
	begin
		//m_axi.init_axi_master();
		fail = 0;
		tst_addr = $urandom();
		tst_data = $urandom();
		mst = new();
      		mst.mst_cfg.burst_type = 2;
      		mst.mst_cfg.burst_length = 1;
		wait(m_axi.axi_rst_n);
		mst.test(fail, tst_addr, tst_data);
      		if(fail>0)
      		begin
         		$display("Error: Mismatch in AXI4 write/read");
         		$stop;
      		end
		$display("Test Passed");
      		$finish;
	end

//endprogram
endmodule

module test();

logic [DBUS_WIDTH-1: 0] test_rd_data;
logic [DBUS_WIDTH-1: 0] test_wr_data;
wire  [DBUS_WIDTH-1:0]  dpram_wr_data;
wire [LOCAL_AWIDTH-1:0] dpram_wr_addr;
wire                    dpram_we;
wire [STRB_SIZE-1:0]    dpram_be;
wire [LOCAL_AWIDTH-1:0] dpram_rd_addr;
wire                    dpram_rd_en;
wire [DBUS_WIDTH-1:0]   dpram_rd_data;

logic                   axi_clk;
logic                   axi_rst_n;

always #10 axi_clk = ~axi_clk;

axi_intf #(
._ID_WIDTH(ID_WIDTH),
._AWIDTH(`AWIDTH),
._STRB_SIZE(STRB_SIZE),
._DBUS_WIDTH(DBUS_WIDTH))
axi_bus_if (axi_clk, axi_rst_n);

axi_master_model mst0 (axi_bus_if.axi_master);

	initial
	begin
		axi_clk = 0;
      		axi_rst_n = 0;
		axi_bus_if.init_axi_master();
		#25;
      		axi_rst_n = 1;
	end

axi4_IF_MEM axi4_if_mem
(
  .ACLK(axi_bus_if.axi_clk),
  .ARESETn(axi_bus_if.axi_rst_n),
  .ARVALID(axi_bus_if.axi_arvalid),
  .AWVALID(axi_bus_if.axi_awvalid),
  .BREADY(axi_bus_if.axi_bready),
  .RREADY(axi_bus_if.axi_rready),
  .WLAST(axi_bus_if.axi_wlast),
  .WVALID(axi_bus_if.axi_wvalid),
  .ARID(axi_bus_if.axi_arid),
  .AWID(axi_bus_if.axi_awid),
  //.WID(axi_bus_if.axi_), //axi3
  .ARBURST(axi_bus_if.axi_arburst),
  //.ARLOCK(axi_bus_if.axi_),
  .ARSIZE(axi_bus_if.axi_arsize),
  .AWBURST(axi_bus_if.axi_awburst),
  //.AWLOCK(axi_bus_if.axi_),
  .AWSIZE(axi_bus_if.axi_awsize),
  //.ARPROT(axi_bus_if.axi_),
  //.AWPROT(axi_bus_if.axi_),
  .ARADDR(axi_bus_if.axi_araddr),
  .AWADDR(axi_bus_if.axi_awaddr),
  .WDATA(axi_bus_if.axi_wdata),
  //.ARCACHE(axi_bus_if.axi_),
  .ARLEN(axi_bus_if.axi_arlen),
  //.ARQOS(axi_bus_if.axi_),
  //.AWCACHE(axi_bus_if.axi_),
  .AWLEN(axi_bus_if.axi_awlen),
  //.AWQOS(axi_bus_if.axi_),
  .WSTRB(axi_bus_if.axi_wstrb),
  .ARREADY(axi_bus_if.axi_arready),
  .AWREADY(axi_bus_if.axi_awready),
  .BVALID(axi_bus_if.axi_bvalid),
  .RLAST(axi_bus_if.axi_rlast),
  .RVALID(axi_bus_if.axi_rvalid),
  .WREADY(axi_bus_if.axi_wready),
  .BID(axi_bus_if.axi_bid),
  .RID(axi_bus_if.axi_rid),
  .BRESP(axi_bus_if.axi_bresp),
  .RRESP(axi_bus_if.axi_rresp),
  .RDATA(axi_bus_if.axi_rdata),
  .ram_wr_en(dpram_we),
  .ram_wr_addr(dpram_wr_addr),
  .ram_be(dpram_be),
  .ram_wr_data(dpram_wr_data),
  .ram_rd_en(dpram_rd_en),
  .ram_rd_addr(dpram_rd_addr),
  .ram_rd_data(dpram_rd_data)
);


dp_sram dpram_if
(
	.wr_data(dpram_wr_data),
	.wr_addr(dpram_wr_addr),
	.we(dpram_we),
	.be(dpram_be),
	.clk(axi_clk),
	.rd_addr(dpram_rd_addr),
	.rd_en(dpram_rd_en), 
	.rd_data(dpram_rd_data)
);
endmodule
