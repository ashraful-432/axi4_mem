`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/30/2019 12:34:43 PM
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "AXI4_config.h"

   typedef struct {
        int AWIDTH;
	int DWIDTH;
	int RAM_DEPTH;
   }e_defs_dpi;

module test();

logic                   s_axi_clk;
logic                   s_axi_rst_n;
logic [`AWIDTH - 1 : 0] s_axi_awaddr;
logic [DBUS_WIDTH-1: 0] s_axi_wdata;
logic [DBUS_WIDTH - 1 : 0] test_wr_data;
logic                   s_axi_awvalid;
logic                   s_axi_awready;
logic                   s_axi_wvalid;
logic                   s_axi_wready;
logic                   s_axi_bvalid;
logic                   s_axi_bready;
logic [1 : 0]           s_axi_bresp;
logic [STRB_SIZE-1 : 0] s_axi_wstrb;
logic                   s_axi_wlast;
logic [ID_WIDTH-1:0]    s_axi_awid;
logic [1:0]             s_axi_awburst;
logic [2:0]             s_axi_awsize;
logic [7:0]             s_axi_awlen;
logic [ID_WIDTH-1:0]    s_axi_bid;

logic [`AWIDTH - 1 : 0] s_axi_araddr;
logic [DBUS_WIDTH-1: 0] s_axi_rdata;
logic [DBUS_WIDTH-1: 0] test_rd_data;
logic                   s_axi_arvalid;
logic                   s_axi_arready;
logic                   s_axi_rvalid;
logic                   s_axi_rready;
logic [1 : 0]           s_axi_rresp;
logic [ID_WIDTH-1:0]    s_axi_arid;
logic [1:0]             s_axi_arburst;
logic [2:0]             s_axi_arsize;
logic [7:0]             s_axi_arlen;
logic                   s_axi_rlast;
logic [ID_WIDTH-1:0]    s_axi_rid;

wire [DBUS_WIDTH-1:0]   dpram_wr_data;
wire [LOCAL_AWIDTH-1:0] dpram_wr_addr;
wire                    dpram_we;
wire [STRB_SIZE-1:0]    dpram_be;
wire [LOCAL_AWIDTH-1:0] dpram_rd_addr;
wire                    dpram_rd_en;
wire [DBUS_WIDTH-1:0]   dpram_rd_data;

   always #10 s_axi_clk = ~s_axi_clk;
   
   import "DPI-C" context task c_axi_write(input e_defs_dpi defs, input int mode, input int addr, input int tst_data, input int Burst_Length);
   //import "DPI-C" context task c_axi_read (input int addr, output int tst_data, input int n_transaction);
   import "DPI-C" context task c_axi_read (input e_defs_dpi defs, input int mode, input int addr, input int expected_data, input int Burst_Length, output int fail);
   // Write task
   export "DPI-C" task wait_negedge;
   export "DPI-C" task wait_posedge;
   export "DPI-C" task assert_aw_channel;
   export "DPI-C" task wait_for_awready;
   export "DPI-C" task deassert_aw_channel;
   export "DPI-C" task assert_wr_channel;
   export "DPI-C" task wait_for_wready;
   export "DPI-C" task deassert_wr_channel;
   export "DPI-C" task assert_bready;
   export "DPI-C" task wait_for_bvalid;
   export "DPI-C" task deassert_bready;
   // Read task
   export "DPI-C" task assert_ar_channel;
   export "DPI-C" task wait_for_arready;
   export "DPI-C" task deassert_ar_channel;
   export "DPI-C" task assert_rready;
   export "DPI-C" task cap_rd_data;
   export "DPI-C" task check_rlast;
   export "DPI-C" task deassert_rready;

   typedef struct {
        bit [1:0] burst_type;
	bit [7:0] burst_length;
   }axi_mst_config;

   class axi_slave_test;
   rand bit[`AWIDTH - 1:0]  addr;
   rand bit[DBUS_WIDTH-1:0] data;
   axi_mst_config mst_cfg;
   e_defs_dpi deflist;

   function new();
   begin
      deflist.AWIDTH = `AWIDTH;
      deflist.DWIDTH = `DWIDTH;
      deflist.RAM_DEPTH = `RAM_DEPTH;
   end
   endfunction
    
   task test;
   output int tst_fail;
   automatic int tst_addr;
   automatic int tst_data; 
   begin
      addr = $urandom();
      data = $urandom();
      tst_addr = { addr[`AWIDTH-1:`DWIDTH], {`DWIDTH{1'b0}} };
      tst_data = data;
      @(negedge s_axi_clk);
      c_axi_write(deflist, mst_cfg.burst_type, tst_addr, tst_data, mst_cfg.burst_length);
      #25;
      @(negedge s_axi_clk);
      c_axi_read(deflist, mst_cfg.burst_type, tst_addr, tst_data, mst_cfg.burst_length, tst_fail); 
      if(tst_fail>0)
      begin
         $display("Error: Mismatch in AXI4 read");
      end
   end
   endtask  
   endclass: axi_slave_test
   
   task wait_negedge;
   begin
    @(negedge s_axi_clk);
   end
   endtask
   
   task wait_posedge;
   begin
    @(posedge s_axi_clk);
   end
   endtask
   
   task assert_aw_channel;
   input int addr;
   input int awid;//logic [ID_WIDTH-1:0] awid;
   input int awlen;//logic [7:0] awlen;
   input int awsize;//logic [2:0] awsize;
   input int awburst;//logic [2:0] awburst;
   begin
    $display("TASK: assert_aw_channel:: Addr = 0x%4x Awid = 0x%4x Awlen = 0x%x Awsize = 0x%x Awburst = 0x%x Time = %d\n",
                  addr, awid, awlen, awsize, awburst, $time);
    //@(negedge s_axi_clk);
    s_axi_awaddr  = addr;
    s_axi_awid    = awid;         
    s_axi_awvalid = 1;
    s_axi_awlen   = awlen;
    s_axi_awsize  = awsize;
    s_axi_awburst = awburst;
   end
   endtask  
   
   task wait_for_awready;
   automatic int time_out = 0;
   begin
    do
    begin
        @(posedge s_axi_clk);
        time_out = time_out+1;
        if(time_out>64)
        begin
           $display("TASK: wait_for_awready:: Timeout without asserting slaves AXI AWREADY\n");
           $stop;
        end
    end while(s_axi_awready != 1);
    time_out = 0;
   end
   endtask
   
   task deassert_aw_channel;
   begin
    @(negedge s_axi_clk);
    s_axi_awaddr  = 'bX;
    s_axi_awid    = 'bX;         
    s_axi_awvalid = 0;
    s_axi_awlen   = 'bX;
    s_axi_awsize  = 'bX;
    s_axi_awburst = 'bX;         
   end
   endtask
   
   task assert_wr_channel;
   input int data;//[DBUS_WIDTH-1:0] data;
   input int strobe;
   input logic wlast;
   begin
    s_axi_wvalid = 1;         
    s_axi_wdata = data;
    s_axi_wstrb = strobe;
    s_axi_wlast = wlast;
   end
   endtask
   
   task wait_for_wready;
   automatic int time_out=0;
   begin
    do
    begin
        time_out = time_out+1;
        if(time_out>64)
        begin
            $display("TASK: wait_for_wready:: Timeout without asserting slaves AXI WREADY\n");
            $stop;
        end
        @(posedge s_axi_clk);
    end while(s_axi_wready != 1);
    time_out = 0;
   end
   endtask
   
   task deassert_wr_channel;
   begin
    @(negedge s_axi_clk);
    s_axi_wvalid = 0;         
    s_axi_wdata = 'bX;
    s_axi_wstrb = 'bX;
    s_axi_wlast = 0;  
   end
   endtask
   
   task assert_bready;
   begin
    //@(negedge s_axi_clk);
    s_axi_bready <=1;
   end
   endtask
   
   task wait_for_bvalid;
   automatic int time_out;
   begin
    do
    begin
        @(posedge s_axi_clk);
        time_out = time_out+1;
        if(time_out>64)
        begin
            $display("TASK: wait_for_bvalid:: Timeout without asserting slaves AXI BVALID\n");
            $stop;
        end
    end while(s_axi_bvalid != 1);
   end
   endtask
   
   task deassert_bready;
   begin
    @(negedge s_axi_clk);
    s_axi_bready <=0;
   end
   endtask

   task assert_ar_channel;
   input int address;
   input int arid;//logic [ID_WIDTH-1:0] arid;
   input int arlen;//logic [7:0] arlen;
   input int arsize;//logic [2:0] arsize;
   input int arburst;//logic [2:0] arburst;
   begin
    s_axi_araddr  = address;
    s_axi_arid    = arid;
    s_axi_arlen   = arlen;
    s_axi_arsize  = arsize;
    s_axi_arburst = arburst;
    s_axi_arvalid = 1;
   end
   endtask

   task wait_for_arready;
   automatic int time_out = 0;
   begin
      do
      begin
         time_out = time_out+1;
         if(time_out>64)
         begin
           $display("TASK: single_read:: Timeout without asserting slaves AXI ARREADY\n");
           $stop;
         end
         @(posedge s_axi_clk);         
      end while(s_axi_arready != 1);
      time_out = 0;    
   end
   endtask

   task deassert_ar_channel;
   begin
    @(negedge s_axi_clk);
    s_axi_araddr  = 'bX;
    s_axi_arvalid = 0;
   end
   endtask

   task assert_rready;
   begin
     s_axi_rready = 1;
   end
   endtask

   task cap_rd_data;
   output int data;
   automatic int time_out = 0;
   begin
      do
      begin
         time_out = time_out+1;
         if(time_out>64)
         begin
           $display("TASK: single_read:: Timeout without asserting slaves AXI RVALID\n");
           $stop;
         end
         @(posedge s_axi_clk);         
      end while(s_axi_rvalid != 1);
      if(s_axi_rresp != 2'b00) 
      begin
	    $display("Error: Read response return non zero values (%x) ", s_axi_rresp);
            $stop;
      end
      data = s_axi_rdata;
   end
   endtask

   task check_rlast;
   begin
      if(s_axi_rlast != 1)
      begin
         $display("TASK: single_read:: Eroor not asserted slaves AXI RLAST \n");
         $stop;
      end
   end
   endtask

   task deassert_rready;
   begin
     @(negedge s_axi_clk);
     s_axi_rready = 1'b0;
   end
   endtask

   //e_defs_dpi deflist;
   axi_slave_test mst0;
   int fail;

   initial 
   begin
      s_axi_clk = 0;
      s_axi_rst_n = 0;
	  
      s_axi_awid    = 0;
      s_axi_awaddr  = 0;
      s_axi_awlen   = 0;
      s_axi_awsize  = 0;
      s_axi_awburst = 0;
      //s_axi_awlock  <= 0;
      //s_axi_awcache <= 0;
      //s_axi_awprot  <= 0;
      s_axi_awvalid = 0;
      //s_axi_wid    <= 0;
      s_axi_wdata   = 0;
      s_axi_wstrb   = 0;
      s_axi_wlast   = 0;
      s_axi_wvalid  = 0;
      //s_axi_bid     = 0;
      //s_axi_bresp   = 0;
      s_axi_bready  = 0;

      s_axi_arid    = 0;
      s_axi_araddr  = 0;
      s_axi_arlen   = 0;
      s_axi_arsize  = 0;
      //s_axi_arlock  <= 0;
      //s_axi_arcache <= 0;
      //s_axi_arprot  <= 0;
      s_axi_arvalid = 0;
      s_axi_rready  = 0; 
      fail = 0;
      #25;
      s_axi_rst_n = 1;
      @(negedge s_axi_clk) #3;
      mst0 = new();
      mst0.mst_cfg.burst_type = 2;
      mst0.mst_cfg.burst_length = 1;
      mst0.test(fail);
      if(fail>0)
      begin
         $display("Error: Mismatch in AXI4 write/read");
         $stop;
      end
      for(integer i=1; i<5; i++)
      begin
        @(negedge s_axi_clk) #3;
        mst0.mst_cfg.burst_type = 2;
        mst0.mst_cfg.burst_length = 2**i;
        mst0.test(fail);
        if(fail>0)
        begin
           $display("Error: Mismatch in AXI4 write/read");
           $stop;
        end
      end

      $display("Test Passed");
      $finish;
   end
   
    
axi4_IF_MEM axi4_if_mem
(
  .ACLK(s_axi_clk),
  .ARESETn(s_axi_rst_n),
  .ARVALID(s_axi_arvalid),
  .AWVALID(s_axi_awvalid),
  .BREADY(s_axi_bready),
  .RREADY(s_axi_rready),
  .WLAST(s_axi_wlast),
  .WVALID(s_axi_wvalid),
  .ARID(s_axi_arid),
  .AWID(s_axi_awid),
  //.WID(s_axi_), //axi3
  .ARBURST(s_axi_arburst),
  //.ARLOCK(s_axi_),
  .ARSIZE(s_axi_arsize),
  .AWBURST(s_axi_awburst),
  //.AWLOCK(s_axi_),
  .AWSIZE(s_axi_awsize),
  //.ARPROT(s_axi_),
  //.AWPROT(s_axi_),
  .ARADDR(s_axi_araddr),
  .AWADDR(s_axi_awaddr),
  .WDATA(s_axi_wdata),
  //.ARCACHE(s_axi_),
  .ARLEN(s_axi_arlen),
  //.ARQOS(s_axi_),
  //.AWCACHE(s_axi_),
  .AWLEN(s_axi_awlen),
  //.AWQOS(s_axi_),
  .WSTRB(s_axi_wstrb),
  .ARREADY(s_axi_arready),
  .AWREADY(s_axi_awready),
  .BVALID(s_axi_bvalid),
  .RLAST(s_axi_rlast),
  .RVALID(s_axi_rvalid),
  .WREADY(s_axi_wready),
  .BID(s_axi_bid),
  .RID(s_axi_rid),
  .BRESP(s_axi_bresp),
  .RRESP(s_axi_rresp),
  .RDATA(s_axi_rdata),
  .ram_wr_en(dpram_we),
  .ram_wr_addr(dpram_wr_addr),
  .ram_be(dpram_be),
  .ram_wr_data(dpram_wr_data),
  .ram_rd_en(dpram_rd_en),
  .ram_rd_addr(dpram_rd_addr),
  .ram_rd_data(dpram_rd_data)
);

dp_sram dpram_if
(
	.wr_data(dpram_wr_data),
	.wr_addr(dpram_wr_addr),
	.we(dpram_we),
	.be(dpram_be),
	.clk(s_axi_clk),
	.rd_addr(dpram_rd_addr),
	.rd_en(dpram_rd_en), 
	.rd_data(dpram_rd_data)
);
endmodule
