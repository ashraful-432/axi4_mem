#include "stdio.h"
#include "stdlib.h"
#include "svdpi.h"
//DPI_DLLESPEC

#define FIXED 0
#define INCR  1
#define WRAP  2

#define TRUE   1
#define FALSE  0

typedef struct exchange_defs_w_dpi {
	int AWIDTH;
	int DWIDTH;
	int RAM_DEPTH;
} e_defs_dpi; 


unsigned int my_log2( unsigned int x )
{
  unsigned int ans = 0 ;
  if(x==1) return 0;
  while( x>>=1 ) ans++;
  return ans ;
}

void c_axi_write(const e_defs_dpi *defs, const int mode, const int addr, const int tst_data, const int Burst_Length)
{
	printf("DPI-C: c_axi_write:: initiating write operation at addr = %x with %d transactions\n", addr, Burst_Length);
	int Number_Bytes = (1<<defs->DWIDTH); //The maximum number of bytes in each data transfer
	int Data_Bus_Bytes = (1<<defs->DWIDTH); //The number of byte lanes in the data bus.
	int Aligned_Address = (addr/Number_Bytes) * Number_Bytes;
	int Address_N; //The address of transfer N in a burst. N is 1 for the first transfer in a burst.
	int Lower_Byte_Lane=0; //The byte lane of the lowest addressed byte of a transfer
	int Upper_Byte_Lane=0; //The byte lane of the highest addressed byte of a transfer
	int aligned; // Check whether addr is aligned to nbytes
	if(Aligned_Address == addr)
		aligned = TRUE;
	else
		aligned = FALSE; 
	int dtsize = Number_Bytes * Burst_Length; // Maximum total data transaction size
	int Lower_Wrap_Boundary=0, Upper_Wrap_Boundary=0;
	int strobe;
	int tst_wr_data = tst_data;
	int tst_addr = addr;
	if (mode == WRAP)
	{
		Lower_Wrap_Boundary = (addr/dtsize) * dtsize;
		// addr must be aligned for a wrapping burst
		Upper_Wrap_Boundary = Lower_Wrap_Boundary + dtsize;
	}
	int awid = rand();//0;
	int awlen = Burst_Length-1;
	/*if(mode == INCR)
		awlen = Burst_Length;
	else
		awlen = my_log2(Burst_Length);*/
	int awsize = my_log2(Data_Bus_Bytes);
	int awburst = mode;
	wait_negedge();
	assert_aw_channel(addr, awid, awlen, awsize, awburst);
	wait_for_awready();
	deassert_aw_channel();
	int n, i;
	for (n = 1; n<=Burst_Length; n++)
	{
		printf("DPI-C: c_axi_write:: \ttransaction_%d\n",n);
		Lower_Byte_Lane = addr - ((addr/Data_Bus_Bytes) * Data_Bus_Bytes);
		if (aligned)
		{
			Upper_Byte_Lane = Lower_Byte_Lane + Number_Bytes - 1;
		}
		else
		{
			Upper_Byte_Lane = Aligned_Address + Number_Bytes - 1 - ((addr/Data_Bus_Bytes) * Data_Bus_Bytes);
		}
		strobe = 0; // (1<<Data_Bus_Bytes) - 1;
		for(i=Lower_Byte_Lane; i<=Upper_Byte_Lane; i++)
			strobe = strobe | (1<<i);
		tst_wr_data = tst_wr_data << n-1;
		if(n==Burst_Length)
			assert_wr_channel(tst_wr_data, strobe, 1);
		else
			assert_wr_channel(tst_wr_data, strobe, 0);
		wait_for_wready();
		// Increment address if necessary
		//if (mode != FIXED)
		if (aligned)
		{
			tst_addr = tst_addr + Number_Bytes;
			if (mode == WRAP)
			{
				// WRAP mode is always aligned
				if (tst_addr >= Upper_Wrap_Boundary)
					tst_addr = Lower_Wrap_Boundary;
			}
		}
		else
		{
			tst_addr = Aligned_Address + Number_Bytes;
			aligned = TRUE; // All transfers after the first are aligned
		}
	}
	//assert_wr_channel(tst_data, strobe, 1);
	//wait_for_wready();
	deassert_wr_channel();
	assert_bready();
	wait_for_bvalid();
	deassert_bready();
}

void c_axi_read(const e_defs_dpi *defs, const int mode, const int addr, const int expected_data, const int Burst_Length, int *fail)
{
	printf("DPI-C: c_axi_write:: initiating read operation at addr = %x with %d transactions\n", addr, Burst_Length);
	*fail = 0;
	int Number_Bytes = (1<<defs->DWIDTH); //The maximum number of bytes in each data transfer
	int Data_Bus_Bytes = (1<<defs->DWIDTH); //The number of byte lanes in the data bus.
	int Aligned_Address = (addr/Number_Bytes) * Number_Bytes;
	int Address_N; //The address of transfer N in a burst. N is 1 for the first transfer in a burst.
	int Lower_Byte_Lane=0; //The byte lane of the lowest addressed byte of a transfer
	int Upper_Byte_Lane=0; //The byte lane of the highest addressed byte of a transfer
	int aligned; // Check whether addr is aligned to nbytes
	if(Aligned_Address == addr)
		aligned = TRUE;
	else
		aligned = FALSE; 
	int dtsize = Number_Bytes * Burst_Length; // Maximum total data transaction size
	int Lower_Wrap_Boundary=0, Upper_Wrap_Boundary=0;
	int strobe;
	int tst_addr = addr;
	int *tst_rd_data = (int*) malloc(sizeof(int));// 
	int exp_data = expected_data;
	if (mode == WRAP)
	{
		Lower_Wrap_Boundary = (addr/dtsize) * dtsize;
		// addr must be aligned for a wrapping burst
		Upper_Wrap_Boundary = Lower_Wrap_Boundary + dtsize;
	}
	int arid = rand();//0;
	int arlen = Burst_Length-1;
	int arsize = my_log2(Data_Bus_Bytes);
	int arburst = mode;
	wait_negedge();
	assert_ar_channel(addr, arid, arlen, arsize, arburst);
	wait_for_arready();
	deassert_ar_channel();
	wait_negedge();
	int n, i;
	for (n = 1; n<=Burst_Length; n++)
	{
		printf("DPI-C: c_axi_read:: \ttransaction_%d\n",n);
		Lower_Byte_Lane = addr - ((addr/Data_Bus_Bytes) * Data_Bus_Bytes);
		if (aligned)
		{
			Upper_Byte_Lane = Lower_Byte_Lane + Number_Bytes - 1;
		}
		else
		{
			Upper_Byte_Lane = Aligned_Address + Number_Bytes - 1 - ((addr/Data_Bus_Bytes) * Data_Bus_Bytes);
		}
		strobe = 0; // (1<<Data_Bus_Bytes) - 1;
		for(i=Lower_Byte_Lane; i<=Upper_Byte_Lane; i++)
			strobe = strobe | (1<<i);
		assert_rready();
		cap_rd_data(tst_rd_data);
		exp_data = (exp_data<<n-1);
		if(*tst_rd_data != exp_data)
		{
			printf("DPI-C: Error:: Read Transaction Failed, expected %x but received %x\n", exp_data, *tst_rd_data );
			*fail = 1;
			//return;
		}
		if(n==Burst_Length)
			check_rlast();
		// Increment address if necessary
		//if (mode != FIXED)
		if (aligned)
		{
			tst_addr = tst_addr + Number_Bytes;
			if (mode == WRAP)
			{
				// WRAP mode is always aligned
				if (tst_addr >= Upper_Wrap_Boundary)
					tst_addr = Lower_Wrap_Boundary;
			}
		}
		else
		{
			tst_addr = Aligned_Address + Number_Bytes;
			aligned = TRUE; // All transfers after the first are aligned
		}
	}
	deassert_rready();
}

#if 0
void c_axi_read(int addr, int *tst_data, int Burst_Length)
{
	int aligned_address;
	int arid = 0;
	int arlen = my_log2(Burst_Length);
	int arsize = 4;//my_log2(strb_size);
	int arburst = 1;
	wait_negedge();
	assert_ar_channel(addr, arid, arlen, arsize, arburst);
	wait_for_arready();
	deassert_ar_channel();
	wait_negedge();
	assert_rready();
	cap_rd_data(tst_data);
	check_rlast();
	deassert_rready();
}
#endif
