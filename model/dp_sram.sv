`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/30/2019 11:55:12 AM
// Design Name: 
// Module Name: dp_sram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "AXI4_config.h"

module dp_sram
(
	input [DBUS_WIDTH-1:0]        wr_data,
	input [LOCAL_AWIDTH-1:0]      wr_addr,
	input                         we,
	input [STRB_SIZE-1:0]         be,
	input                         clk,
	input [LOCAL_AWIDTH-1:0]      rd_addr,
	input                         rd_en,
	output reg [DBUS_WIDTH-1:0]   rd_data
);
    // Declare the RAM variable
    genvar i, j;
	reg [7:0] ram [STRB_SIZE-1:0] [RAM_SIZE-1:0];
	logic [7:0] temp [STRB_SIZE-1:0];
generate
for(i=0; i<STRB_SIZE; i++) 
begin
    //always_latch
    //begin
	   assign temp[i] = wr_data[(8*i)+7:(8*i)];
    //end
end
endgenerate
	// Port A
	always @ (posedge clk)
	begin
		if (we)
		begin
			for(integer c=0; c<STRB_SIZE; c=c+1) 
			begin: write_model
                if(be[c]==1)
					  ram[c] [wr_addr] = temp[c];
			end
		end
	end

	// Port B
generate
for(i=0; i<STRB_SIZE; i++) begin
	always@*//_latch //@ (posedge clk)
	begin
	  if(rd_en)
		  rd_data[(8*i)+7:(8*i)] <= ram [i] [rd_addr];
	end
end
endgenerate

integer di,dj;
initial begin
  for(di=0;di<STRB_SIZE;di++) begin
    for(dj=0;dj<RAM_SIZE;dj++) begin
      ram[di][dj] = 0;//$urandom_range(8'h00, 8'hff);
    end
  end
end

endmodule
