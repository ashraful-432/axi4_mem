`ifndef AWIDTH
	`define AWIDTH 16
`endif
`ifndef DWIDTH
	`define DWIDTH 2 //2^2 = 4Byte
`endif
`ifndef RAM_DEPTH
    `define RAM_DEPTH 13 //2^13 = 8KB
`endif

parameter STRB_SIZE = (1<<`DWIDTH);
parameter DBUS_WIDTH = (1<<`DWIDTH)*8;
parameter LOCAL_AWIDTH = (`RAM_DEPTH - `DWIDTH);
parameter ID_WIDTH = 2;
  //parameter axi_lock_width = 2;
  //parameter axi_prot_width = 2;
  //parameter axi_cache_width = 2;
  //parameter axi_qos_width = 2;

parameter RAM_BE_SIZE = (1<<`DWIDTH);
parameter RAM_DWIDTH  = (1<<`DWIDTH)*8;
parameter RAM_AWIDTH  = (`RAM_DEPTH - `DWIDTH);
parameter RAM_SIZE    = (1<<`RAM_DEPTH)/(1<<`DWIDTH);

