//`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/29/2019 08:15:30 PM
// Design Name: 
// Module Name: axi4_IF_MEM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "AXI4_config.h"

module axi4_IF_MEM(
ACLK,
ARESETn,

AWID,
AWADDR,
AWLEN,
AWSIZE,
AWBURST,
AWVALID,
AWREADY,

WDATA,
WSTRB,
WLAST,
WVALID,
WREADY,

BID,
BRESP,
BVALID,
BREADY,

ARID,
ARADDR,
ARLEN,
ARSIZE,
ARBURST,
ARVALID,
ARREADY,

RID,
RDATA,
RRESP,
RLAST,
RVALID,
RREADY,

ram_wr_en,
ram_wr_addr,
ram_be,
ram_wr_data,
ram_rd_en,
ram_rd_addr,
ram_rd_data
  );
  
localparam IDLE_W      = 2'b00;
localparam WR_BURST    = 2'b01;
localparam WAIT_4_RESP = 2'b10;

localparam IDLE_RD      = 2'b00;
localparam FILL_RD_PIPE = 2'b01;
localparam RD_BURST     = 2'b10;

   input                                  ACLK;
   input                                  ARESETn;

   input  [ID_WIDTH-1:0]                  AWID;
   input  [`AWIDTH-1:0]                   AWADDR;
   input  [7:0]                           AWLEN;
   input  [2:0]                           AWSIZE;
   input  [1:0]                           AWBURST;
   input                                  AWVALID;
   output logic                           AWREADY;
   //input logic [axi_lock_width-1:0]      AWLOCK,
   //input logic [axi_prot_width-1:0]      AWPROT,
   //input logic [axi_cache_width-1:0]     AWCACHE,
   //input logic [axi_qos_width-1:0]       AWQOS,

   input  [DBUS_WIDTH-1:0]                WDATA;
   input  [STRB_SIZE-1:0]                 WSTRB;
   input                                  WLAST;
   input                                  WVALID;
   output logic                           WREADY;

   output logic [ID_WIDTH-1:0]            BID;
   output logic [1:0]                     BRESP;
   output logic                           BVALID;
   input                                  BREADY;
   //input  [ID_WIDTH-1:0]                  WID; //AXI3

   input  [ID_WIDTH-1:0]                  ARID;
   input  [`AWIDTH-1:0]                   ARADDR;
   input  [7:0]                           ARLEN;
   input  [2:0]                           ARSIZE;
   input  [1:0]                           ARBURST;
   input                                  ARVALID;
   output logic                           ARREADY;
   //input logic [axi_cache_width-1:0]     ARCACHE,
   //input logic [axi_lock_width-1:0]      ARLOCK,
   //input logic [axi_prot_width-1:0]      ARPROT,
   //input logic [axi_qos_width-1:0]       ARQOS,

   output logic [ID_WIDTH-1:0]            RID;
   output logic [DBUS_WIDTH-1:0]          RDATA;
   output logic [1:0]                     RRESP;
   output logic                           RLAST;
   output logic                           RVALID;
   input                                  RREADY;

   // RAM Interface signals
   output logic                    ram_wr_en;
   output logic [LOCAL_AWIDTH-1:0] ram_wr_addr; // Driven from wr_addr
   output logic [STRB_SIZE-1:0]    ram_be;      // alias w/ WSTRB
   output logic [DBUS_WIDTH-1:0]   ram_wr_data; // alias w/ WDATA
   output logic                    ram_rd_en;
   output logic [LOCAL_AWIDTH-1:0] ram_rd_addr; // Driven from rd_addr
   input  logic [DBUS_WIDTH-1:0]   ram_rd_data; // alias w/ RDATA
   
   logic [`AWIDTH-1:0]  wr_addr;  // AWADDR alias
   logic [ID_WIDTH-1:0] wr_id;    // AWID alias
   logic [7:0]          wr_len;   // AWLEN alias
   logic [1:0]          wr_size;  // AWSIZE alias
   logic [1:0]          wr_burst; // AWBURST alias 
   logic [7:0]          wr_cnt;   // counter for burst length
   logic [7:0]          nxt_wr_cnt;
   logic [`AWIDTH-1:0]  next_wr_addr;
   //logic [`AWIDTH-1:0] align_wr_addr; // for future support of unalligned wr transfer
   
   logic trig_wr_ch;  //Trigger write FSM to indicate AWADDR is captured
   logic wr_proc_r;   //indicate the Write transactionis ongoing
   logic wr_done;     //Write transaction is done
   logic wready_r;    // WREADY alias
   logic nxt_wready;
   logic bvalid_r;    // BVALID alias
   logic nxt_bvalid;
   logic ram_wr_en_w;
   
   logic [`AWIDTH-1:0]  rd_addr;  // ARADDR alias
   logic [ID_WIDTH-1:0] rd_id;    // ARID alias
   logic [7:0]          rd_len;   // ARLEN alias
   logic [1:0]          rd_size;  // ARSIZE alias
   logic [1:0]          rd_burst; // ARBURST alias 
   logic [7:0]          rd_cnt;   // counter for burst length
   logic [7:0]          nxt_rd_cnt;
   logic [7:0]          rd_pipe_cnt;   // counter for burst length
   logic [7:0]          nxt_rd_pipe_cnt;
   logic [`AWIDTH-1:0]  next_rd_addr;
   //logic [`AWIDTH-1:0] align_rd_addr; // for future support of unalligned rd transfer
   logic trig_rd_ch;  //Trigger read FSM to indicate ARADDR is captured
   //logic rd_proc_r;   //indicate the Read transactionis ongoing
   logic rd_done;     // Read transaction is done
   logic rvalid_r;    // RVALID alias
   logic nxt_rvalid;
   logic ram_rd_en_w;
   logic rlast_r;
   logic nxt_rlast;

   reg	[1:0]	pstate_w;
   reg	[1:0]	nstate_w;
   reg	[1:0]	pstate_rd;
   reg	[1:0]	nstate_rd;

   //Signals to check in future
   assign RID = rd_id;
   assign BID = wr_id;
   assign BRESP = 0;
   assign RRESP = 0;
   assign ram_wr_addr = wr_addr[`RAM_DEPTH-1:`DWIDTH];
   assign ram_rd_addr = rd_addr[`RAM_DEPTH-1:`DWIDTH];
   assign RLAST = rlast_r;
   assign RVALID = rvalid_r;

   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        wr_addr <= `AWIDTH'd0;
        wr_id   <= 0;
        wr_len  <= 0;
        wr_size <= 0;
        wr_burst <=0;
    end
    else if(AWVALID && AWREADY)
    begin
        wr_addr <= AWADDR;
        wr_id   <= AWID;
        wr_len  <= AWLEN;
        wr_size <= AWSIZE;
        wr_burst<= AWBURST;
    end
    else
    begin
        wr_addr <= next_wr_addr;
    end
   end
   
   assign trig_wr_ch = AWVALID && AWREADY;
   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        wr_proc_r <= 1'd0;
        wr_cnt    <= 0;
        AWREADY <= 1'd1;
    end
    else if(AWVALID && AWREADY)
    begin
        wr_proc_r <= 1;
        wr_cnt    <= AWLEN;
        AWREADY <= 1'd0;
    end
    else if(wr_done)
    begin
        wr_proc_r <= 1'd0;
        AWREADY <= 1'd1;
        wr_cnt    <= 0;
    end
    else //if(wr_proc_r && !wr_done)
    begin
        wr_cnt <= nxt_wr_cnt;
    end
   end
   
   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        wready_r <= 1'b0;
        bvalid_r <= 1'b0;
    end
    else
    begin
        wready_r <= nxt_wready;
        bvalid_r <= nxt_bvalid;
    end
   end
   assign WREADY = wready_r;
   assign BVALID = bvalid_r;
   
   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        ram_be      <= 0;
        ram_wr_data <= 0;
        ram_wr_en   <= 0;
    end
    else if (WVALID && WREADY) //(WVALID && nxt_wready) 
    begin
        ram_be      <= WSTRB;
        ram_wr_data <= WDATA;
        ram_wr_en   <= ram_wr_en_w;
    end
    else
    begin
       ram_wr_en   <= ram_wr_en_w;
    end
   end
      
   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        pstate_w <= IDLE_W;
    end
    else
    begin
        pstate_w <= nstate_w;
    end
   end
   
   always_comb
   begin
    nstate_w = IDLE_W;
    ram_wr_en_w = 1'b0;

    next_wr_addr = wr_addr;
    nxt_wr_cnt = wr_cnt;
    nxt_wready = 1'b0;
    nxt_bvalid = bvalid_r && !BREADY;
    wr_done = 1'b0;

    case (pstate_w)
        IDLE_W: 
        begin
            if(trig_wr_ch) 
            begin
                next_wr_addr = AWADDR;
                nxt_wr_cnt = AWLEN;
                nxt_wready = 1'b1;
                nstate_w = WR_BURST;
            end 
            else 
            begin
                nstate_w = IDLE_W;
            end
        end
        WR_BURST: 
        begin
            nxt_wready = 1'b1;
            if(WREADY && WVALID)
            begin
                ram_wr_en_w = 1'b1;
                if(wr_cnt!=0 && ram_wr_en) //++ to make sure if wvalid arrive after wready then ram write does not missed
                begin
                        next_wr_addr = wr_addr + (1<<wr_size); // Narrow burst is not supported yet
                        nxt_wr_cnt = wr_cnt - 1;
                end
                if(wr_cnt!=0)
                begin
                    nstate_w = WR_BURST;
                end 
                else 
                begin
                    nxt_wready = 1'b0;
                    if(BREADY) // is it necessary to check BVALID as well?
                    begin
                        nxt_bvalid = 1'b1;
                        wr_done = 1'b1;
                        nstate_w = IDLE_W;
                    end 
                    else 
                    begin
                        nxt_bvalid = 1'b1; //++
                        nstate_w = WAIT_4_RESP;
                    end
                end
            end 
            else 
            begin
                if(wr_cnt==0 && ram_wr_en) //++ to make sure if wvalid goes off early after write finished it can transit to next state
                begin
                    if(BREADY) 
                    begin
                        nxt_wready = 1'b0;
                        nxt_bvalid = 1'b1; //++
                        wr_done = 1'b1;
                        nstate_w = IDLE_W;
                    end
                    else
                    begin
                        nxt_wready = 1'b0;
                        nxt_bvalid = 1'b1; //++
                        nstate_w = WAIT_4_RESP;
                    end
                end
                else
                begin
                   nstate_w = WR_BURST;
                end
            end
        end
        WAIT_4_RESP: begin
            if(BREADY && BVALID)
            begin
                nxt_bvalid = 1'b0; //++
                wr_done = 1'b1;
                nstate_w = IDLE_W;
            end 
            else 
            begin
                nxt_bvalid = 1'b1; //++
                nstate_w = WAIT_4_RESP;
            end
        end
    endcase
   end

   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        rd_addr  <= `AWIDTH'd0;
        rd_id    <= 0;
        rd_len   <= 0;
        rd_size  <= 0;
        rd_burst <=0;
    end
    else if(ARVALID && ARREADY)
    begin
        rd_addr  <= ARADDR;
        rd_id    <= ARID;
        rd_len   <= ARLEN;
        rd_size  <= ARSIZE;
        rd_burst <= ARBURST;
    end
    else
    begin
        rd_addr <= next_rd_addr;
    end
   end

   assign trig_rd_ch = ARVALID && ARREADY;
   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        rd_cnt  <= 0;
        rd_pipe_cnt <= 1;
        ARREADY <= 1'd1;
    end
    else if(ARVALID && ARREADY)
    begin
        rd_cnt  <= ARLEN;
        rd_pipe_cnt <= ARLEN + 1;
        ARREADY <= 1'd0;
    end
    else if(rd_done)
    begin
        ARREADY <= 1'd1;
        rd_cnt  <= 0;
        rd_pipe_cnt <= 1;
    end
    else //if(rd_proc_r && !rd_done)
    begin
        rd_cnt  <= nxt_rd_cnt;
        rd_pipe_cnt <= nxt_rd_pipe_cnt;
    end
   end
   
   always_ff @(posedge ACLK or ARESETn) 
   begin
    if (!ARESETn) 
    begin
        pstate_rd <= IDLE_RD;
        rvalid_r  <= 1'b0;
        rlast_r   <= 1'b0;
    end 
    else 
    begin
        pstate_rd <= nstate_rd;
        rvalid_r <= nxt_rvalid;
        rlast_r  <= nxt_rlast;
    end
   end
   
   always_ff @(posedge ACLK or ARESETn)
   begin
    if(!ARESETn)
    begin
        RDATA     <= 0;
        ram_rd_en <= 1'b0;
    end
    else if ((pstate_rd==FILL_RD_PIPE)||(RREADY && ram_rd_en)) 
    begin
        RDATA     <= ram_rd_data;
        ram_rd_en <= ram_rd_en_w;
    end
    else
    begin
       ram_rd_en <= ram_rd_en_w;
    end
   end
   
   always_comb// @* 
   begin
    nstate_rd = IDLE_RD;

    ram_rd_en_w = 1'b0;
    nxt_rlast = rlast_r;
    nxt_rvalid = 1'b0;
    next_rd_addr = rd_addr;
    nxt_rd_cnt = rd_cnt;
    nxt_rd_pipe_cnt = rd_pipe_cnt;
    rd_done = 1'b0;

    case (pstate_rd)
        IDLE_RD: 
        begin
            if (trig_rd_ch)
            begin
                nxt_rd_cnt = ARLEN;
                ram_rd_en_w = 1'b1; //++
                nstate_rd = FILL_RD_PIPE;
            end 
            else 
            begin
                nstate_rd = IDLE_RD;
            end
        end
        FILL_RD_PIPE:
        begin
            nxt_rvalid = 1'b1;
            if (rd_cnt != 0) 
            begin
                next_rd_addr = rd_addr + (1 << rd_size);
                nxt_rd_cnt = rd_cnt - 1;
                ram_rd_en_w = 1'b1;
            end
            nxt_rlast = rd_pipe_cnt == 1;
            nstate_rd = RD_BURST;
        end
        RD_BURST: 
        begin
            if(RREADY)
            begin
                if (rd_cnt != 0) 
                begin
                    next_rd_addr = rd_addr + (1 << rd_size);
                    nxt_rd_cnt = rd_cnt - 1;
                end
                if(rd_pipe_cnt!=1)
                begin
                    nxt_rd_pipe_cnt = rd_pipe_cnt - 1;
                end
                //?????nxt_rlast = rd_pipe_cnt == 1;
                if(rd_pipe_cnt==1)
                begin
                    rd_done = 1'b1;
                    ram_rd_en_w = 1'b0;
                    nxt_rvalid = 1'b0;
                    nxt_rlast = 1'b0;
                    nstate_rd = IDLE_RD;
                end
                else 
                begin
                    ram_rd_en_w = 1'b1;
                    nxt_rvalid = 1'b1;
                    nxt_rlast = rd_pipe_cnt == 2;
                    nstate_rd = RD_BURST;
                end
            end
            else
            begin
                ram_rd_en_w = 1'b1;
                nxt_rvalid = 1'b1;
                nxt_rlast = rd_pipe_cnt == 2;
                nstate_rd = RD_BURST;
            end
        end
    endcase
end
   
endmodule
