# AXI4 RAM (Dual-Port*) Controller

## Introduction

The AXI RAM Controller core can be configured for different address width (from 4 to 32 bits) and data width (8, 16, 32,64, 128 bits). (**\*will support Single Port RAM in future with arbiter**) 
All communication with AXI master devices is performed through a five channel AXI interface. 

All write operations are initiated on the Write Address Channel (AW) of the AXI bus which specifies the type of write transaction and the corresponding address information. 
The Write Data Channel (W) communicates all write data for the single or burst write operations. The Write Response Channel (B) is used as the handshaking or response on the write operation.

On read operations, the Read Address Channel (AR) communicates all address and control information when the AXI master requests a read transfer. 
The AXI slave Controller IP responds on the Read Address Channel (AR) when the read operation can be processed. 
When the read data is available to send back to the AXI master, the Read Data Channel (R) translates the data and status of the operation.

## Features

- Support for 8, 16, 32, 64, 128, 256, 512, and 1024-bit AXI (as well as RAM) data widths. (But the testbench currently supports upto 32-bit due to integer limit, **will improve this in future**)
- Support for all AXI4 burst types and sizes
    - Fixed burst type is treated as INCR burst type
    - INCR burst type can support upto 256 Busts
    - For WRAP transfer wrap address should be aligned to Data bus width
    - Narrow burst is not supported (**will add in future**)
    - Unaligned bust transfer is not supported yet (**will add in future**)
- All transactions are processed in order regardless of ID value. No read reordering or write reordering is implemented.
- No caching or buffering functionality is supported.

## Configuration 

All the configuraion should be defined into AXI4_config.h file in src/ directory. Following should be defined-

- AWIDTH
> AXI4 address bus width configuration. This is Byte addressable address. For an example, even the data bus is 4-Byte (32-bit) the Address value 0 points to Byte 0, Address 1 points to Byte 1 and so on.
- DWIDTH
> AXI4 as well as RAM bus width configuration. Data bus width is calculated as (2^DWIDTH)x8 bits. For an example if DWIDTH is 3 then Data bus with is (2^3)x8 = 64-bit or 8-Byte
- RAM_DEPTH
> Size of interfaced RAM(in Bytes). The RAM size is calculated as (2^RAM_DEPTH) Byte. For an example, if RAM_DEPTH is defined as 13 then the RAM size is (2^13) = 8KByte.

Rest of the values are calculated from above mentioned configurations.

## TestBench 

AXI master's Read and Write transaction is modeled in C language which can be act as driver of the slave's bus. These are imported into the SystemVerilog as task.

  `import "DPI-C" context task c_axi_write(input e_defs_dpi defs, input int mode, input int addr, input int tst_data, input int Burst_Length);`
  
  `import "DPI-C" context task c_axi_read (input e_defs_dpi defs, input int mode, input int addr, input int expected_data, input int Burst_Length, output int fail);`
   
Configuration information is passed as structure e_defs_dpi such that
`  typedef struct {
        int AWIDTH;
	int DWIDTH;
	int RAM_DEPTH;
   }e_defs_dpi;`

Variable mode defines the Burst transfer mode and defined as
` #define FIXED 0`
` #define INCR  1`
` #define WRAP  2`

`Burst_Length` argument indicates the number of Burst transfer in AXI channel. 

For write transaction `tst_data` is the write data to be transferred in the first AXI write burst. Subsequent bursts data is calculated by the C function as left shift by 1 bit.

For read transaction `expected_data` is the read data expected to be transferred in the first AXI read burst. Subsequent bursts expected data is calculated by the C function as left shift by 1 bit.

In the SystemVerilog test.sv contains the AXI RAM controller instantiation and Dual Port Ram model instantiation. Test is controlled by the class object `axi_slave_test`. 

```
 typedef struct {
    bit [1:0] burst_type; 
	bit [7:0] burst_length; 
   }axi_mst_config;

   class axi_slave_test;
   rand bit[`AWIDTH - 1:0]  addr;
   rand bit[DBUS_WIDTH-1:0] data;
   axi_mst_config mst_cfg;
   e_defs_dpi deflist;

   function new();
       
   task test;
   output int tst_fail;
   endtask
```

To use this class, first configure the desired burst type and length in `mst_cfg`, and then call the task test which will return does that test pass or fail. 
That task generate a random address and data. After that it calls the DPI-C tasks to write into the same address and read back from that address and check.

## Directories

src/   directory contains the RTL

test/  directory contains testbench and C files

model/ directory contains Dual Port RAM models

sim/   directory contains all the files in same directory with example command to simulate in Modelsim
